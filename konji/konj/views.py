import os

from django.shortcuts import render
from django.template.loader import render_to_string
import json
# Create your views here.
from django.http import HttpResponse
from django.http import JsonResponse
from django.template import loader
#from django.contrib.auth.hashers import make_password
from passlib.hash import pbkdf2_sha256
from django.db import connection
# Create your views here.
from datetime import datetime
from datetime import timedelta

from django.db.models import Q

from .models import *
from django import forms
from django.shortcuts import redirect

from django.views.decorators.csrf import ensure_csrf_cookie
# Create your views here.


max_st_slik = 4

def home(request):
    pasme = Pasma.objects.all().values()
    kategorije = Kategorija.objects.all().values()
    rokovanja = Rokovanje.objects.all().values()
    jahljivosti = Jahljivost.objects.all().values()
    regije = Regija.objects.all().values()

    konji = Konj.objects.all()
    starostMinMin = min(konj.starostKonja or 0 for konj in konji)
    starostMaxMax = max(konj.starostKonja or 0 for konj in konji)
    tezaMinMin = min(konj.tezaKonja or 0 for konj in konji)
    tezaMaxMax = max(konj.tezaKonja or 0 for konj in konji)
    visinaMinMin = min(konj.visinaKonja or 0 for konj in konji)
    visinaMaxMax = max(konj.visinaKonja or 0 for konj in konji)

    ID_Uporabnik = request.session.get('id', -1)
    # preveri sejo
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return render(request, 'konj/home.html', {
            'pasme': pasme,
            'kategorije': kategorije,
            'rokovanja': rokovanja,
            'jahljivosti': jahljivosti,
            'regije': regije,
            'starostMinMin': starostMinMin,
            'starostMaxMax': starostMaxMax,
            'tezaMinMin': tezaMinMin,
            'tezaMaxMax': tezaMaxMax,
            'visinaMinMin': visinaMinMin,
            'visinaMaxMax': visinaMaxMax
        })

    return render(request, 'konj/uporabnik_home.html', {
        'pasme': pasme,
        'kategorije': kategorije,
        'rokovanja': rokovanja,
        'jahljivosti': jahljivosti,
        'regije': regije,
        'starostMinMin': starostMinMin,
        'starostMaxMax': starostMaxMax,
        'tezaMinMin': tezaMinMin,
        'tezaMaxMax': tezaMaxMax,
        'visinaMinMin': visinaMinMin,
        'visinaMaxMax': visinaMaxMax
    })


def search(request):
    pasme = Pasma.objects.all().values()
    kategorije = Kategorija.objects.all().values()
    rokovanja = Rokovanje.objects.all().values()
    jahljivosti = Jahljivost.objects.all().values()
    regije = Regija.objects.all().values()

    pasmaSelected = request.GET.get('pasma', '')
    if pasmaSelected == '':
        pasmaSelected = []
    else:
        pasmaSelected = pasmaSelected.split(',')

    kategorijaSelected = request.GET.get('kategorija', '')
    if kategorijaSelected == '':
        kategorijaSelected = []
    else:
        kategorijaSelected = kategorijaSelected.split(',')

    rokovanjeSelected = request.GET.get('rokovanje', '')
    if rokovanjeSelected == '':
        rokovanjeSelected = []
    else:
        rokovanjeSelected = rokovanjeSelected.split(',')

    jahljivostSelected = request.GET.get('jahljivost', '')
    if jahljivostSelected == '':
        jahljivostSelected = []
    else:
        jahljivostSelected = jahljivostSelected.split(',')

    regijaSelected = request.GET.get('regija', '')
    if regijaSelected == '':
        regijaSelected = []
    else:
        regijaSelected = regijaSelected.split(',')

    starostMin = request.GET.get('starostMin', '')
    starostMax = request.GET.get('starostMax', '')

    tezaMin = request.GET.get('tezaMin', '')
    tezaMax = request.GET.get('tezaMax', '')

    visinaMin = request.GET.get('visinaMin', '')
    visinaMax = request.GET.get('visinaMax', '')

    konji = Konj.objects.all()
    starostMinMin = min(konj.starostKonja or 0 for konj in konji)
    starostMaxMax = max(konj.starostKonja or 0 for konj in konji)
    tezaMinMin = min(konj.tezaKonja or 0 for konj in konji)
    tezaMaxMax = max(konj.tezaKonja or 0 for konj in konji)
    visinaMinMin = min(konj.visinaKonja or 0 for konj in konji)
    visinaMaxMax = max(konj.visinaKonja or 0 for konj in konji)
    #starostMinMin = min(konj.starostKonja for konj in konji)
    #starostMaxMax = max(konj.starostKonja for konj in konji)
    #tezaMinMin = min(konj.tezaKonja for konj in konji)
    #tezaMaxMax = max(konj.tezaKonja for konj in konji)
    #visinaMinMin = min(konj.visinaKonja for konj in konji)
    #visinaMaxMax = max(konj.visinaKonja for konj in konji)

    ID_Uporabnik = request.session.get('id', -1)
    # preveri sejo
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return render(request, 'konj/search.html', {
            'pasme': pasme,
            'kategorije': kategorije,
            'rokovanja': rokovanja,
            'jahljivosti': jahljivosti,
            'regije': regije,
            'pasmaSelected': pasmaSelected,
            'kategorijaSelected': kategorijaSelected,
            'rokovanjeSelected': rokovanjeSelected,
            'jahljivostSelected': jahljivostSelected,
            'regijaSelected': regijaSelected,
            'starostMin': starostMin,
            'starostMax': starostMax,
            'tezaMin': tezaMin,
            'tezaMax': tezaMax,
            'visinaMin': visinaMin,
            'visinaMax': visinaMax,
            'starostMinMin': starostMinMin,
            'starostMaxMax': starostMaxMax,
            'tezaMinMin': tezaMinMin,
            'tezaMaxMax': tezaMaxMax,
            'visinaMinMin': visinaMinMin,
            'visinaMaxMax': visinaMaxMax
        })

    return render(request, 'konj/uporabnik_search.html', {
        'pasme': pasme,
        'kategorije': kategorije,
        'rokovanja': rokovanja,
        'jahljivosti': jahljivosti,
        'regije': regije,
        'pasmaSelected': pasmaSelected,
        'kategorijaSelected': kategorijaSelected,
        'rokovanjeSelected': rokovanjeSelected,
        'jahljivostSelected': jahljivostSelected,
        'regijaSelected': regijaSelected,
        'starostMin': starostMin,
        'starostMax': starostMax,
        'tezaMin': tezaMin,
        'tezaMax': tezaMax,
        'visinaMin': visinaMin,
        'visinaMax': visinaMax,
        'starostMinMin': starostMinMin,
        'starostMaxMax': starostMaxMax,
        'tezaMinMin': tezaMinMin,
        'tezaMaxMax': tezaMaxMax,
        'visinaMinMin': visinaMinMin,
        'visinaMaxMax': visinaMaxMax
    })

def getSearchResults(request):
    pasmaSelected = request.GET.get('pasma', '')
    if pasmaSelected == '':
        pasmaSelected = []
    else:
        pasmaSelected = pasmaSelected.split(',')

    kategorijaSelected = request.GET.get('kategorija', '')
    if kategorijaSelected == '':
        kategorijaSelected = []
    else:
        kategorijaSelected = kategorijaSelected.split(',')

    rokovanjeSelected = request.GET.get('rokovanje', '')
    if rokovanjeSelected == '':
        rokovanjeSelected = []
    else:
        rokovanjeSelected = rokovanjeSelected.split(',')

    jahljivostSelected = request.GET.get('jahljivost', '')
    if jahljivostSelected == '':
        jahljivostSelected = []
    else:
        jahljivostSelected = jahljivostSelected.split(',')

    regijaSelected = request.GET.get('regija', '')
    if regijaSelected == '':
        regijaSelected = []
    else:
        regijaSelected = regijaSelected.split(',')

    starostMin = request.GET.get('starostMin', '')
    starostMax = request.GET.get('starostMax', '')

    tezaMin = request.GET.get('tezaMin', '')
    tezaMax = request.GET.get('tezaMax', '')

    visinaMin = request.GET.get('visinaMin', '')
    visinaMax = request.GET.get('visinaMax', '')

    sorting = request.GET.get('sorting', '')

    my_filter_qs = Q()

    if len(pasmaSelected) > 0:
        my_filter_qs = my_filter_qs & Q(konjID__pasmaID__in=pasmaSelected)

    if len(rokovanjeSelected) > 0:
        my_filter_qs = my_filter_qs & Q(konjID__rokovanjeID__in=rokovanjeSelected)

    if len(jahljivostSelected) > 0:
        my_filter_qs = my_filter_qs & Q(konjID__jahljivostID__in=jahljivostSelected)
    
    if len(regijaSelected) > 0:
        my_filter_qs = my_filter_qs & Q(konjID__regijaID__in=regijaSelected)

    ponudbe = Ponudba.objects.filter(
        my_filter_qs).select_related()

    res = []
    for ponudba in ponudbe:
        row = {}

        row["konj"] = {}
        row["konj"]["imeKonja"] = ponudba.konjID.imeKonja
        row["konj"]["starostKonja"] = ponudba.konjID.starostKonja
        if (row["konj"]["starostKonja"] == None):
            row["konj"]["starostKonja"] = -1
        row["konj"]["tezaKonja"] = ponudba.konjID.tezaKonja
        if (row["konj"]["tezaKonja"] == None):
            row["konj"]["tezaKonja"] = -1
        row["konj"]["visinaKonja"] = ponudba.konjID.visinaKonja
        if (row["konj"]["visinaKonja"] == None):
            row["konj"]["visinaKonja"] = -1

        kategorije = SpadaVKategorijo.objects.filter(konjID=ponudba.konjID.konjID)
        row["konj"]["kategorije"] = []
        katSelected = False
        for kategorija in kategorije:
            kategorijaRow = {}
            kategorijaRow["imeKategorije"] = kategorija.kategorijaID.imeKategorije
            kategorijaRow["opisKategorije"] = kategorija.kategorijaID.opisKategorije
            row["konj"]["kategorije"].append(kategorijaRow)

            if str(kategorija.kategorijaID.kategorijaID) in kategorijaSelected:
                katSelected = True
        
        if not katSelected and len(kategorijaSelected) > 0:
            continue

        row["ponudba"] = {}
        row["ponudba"]["ponudbaID"] = ponudba.ponudbaID
        row["ponudba"]["cena"] = ponudba.cena
        if (row["ponudba"]["cena"] == None):
            row["ponudba"]["cena"] = -1

        row["oseba"] = {}
        row["oseba"]["imeOsebe"] = ponudba.osebaID.imeOsebe
        row["oseba"]["priimekOsebe"] = ponudba.osebaID.priimekOsebe

        slika = SlikeKonj.objects.filter(konjID=ponudba.konjID.konjID).first()
        if slika:
            row["konj"]["slika"] = str(slika.slikaKonja.url).split("/", 1)[1]
        else:
            row["konj"]["slika"] = None

        row["konj"]["rokovanje"] = {}
        row["konj"]["rokovanje"]["imeRokovanja"] = ponudba.konjID.rokovanjeID.imeRokovanja
        row["konj"]["rokovanje"]["rokovanjeOpis"] = ponudba.konjID.rokovanjeID.rokovanjeOpis
        row["konj"]["jahljivost"] = {}
        row["konj"]["jahljivost"]["imeJahljivosti"] = ponudba.konjID.jahljivostID.imeJahljivosti
        row["konj"]["jahljivost"]["jahljivostOpis"] = ponudba.konjID.jahljivostID.jahljivostOpis
        row["konj"]["pasma"] = {}
        row["konj"]["pasma"]["imePasme"] = ponudba.konjID.pasmaID.pasmaIme
        row["konj"]["pasma"]["pasmaOpis"] = ponudba.konjID.pasmaID.pasmaOpis
        row["konj"]["regija"] = {}
        row["konj"]["regija"]["imeRegije"] = ponudba.konjID.regijaID.imeRegije

        res.append(row)

    res = [x for x in res if x["konj"]["starostKonja"] == -1 or (x["konj"]["starostKonja"] >= int(starostMin) and x["konj"]["starostKonja"] <= int(starostMax))]
    res = [x for x in res if x["konj"]["tezaKonja"] == -1 or (x["konj"]["tezaKonja"] >= int(tezaMin) and x["konj"]["tezaKonja"] <= int(tezaMax))]
    res = [x for x in res if x["konj"]["visinaKonja"] == -1 or (x["konj"]["visinaKonja"] >= int(visinaMin) and x["konj"]["visinaKonja"] <= int(visinaMax))]

    if sorting == "cena-":
        res.sort(key=lambda x: x["ponudba"]["cena"], reverse=False)
    elif sorting == "cena+":
        res.sort(key=lambda x: x["ponudba"]["cena"], reverse=True)
    elif sorting == "starost-":
        res.sort(key=lambda x: x["konj"]["starostKonja"], reverse=False)
    elif sorting == "starost+":
        res.sort(key=lambda x: x["konj"]["starostKonja"], reverse=True)
    elif sorting == "teza-":
        res.sort(key=lambda x: x["konj"]["tezaKonja"], reverse=False)
    elif sorting == "teza+":
        res.sort(key=lambda x: x["konj"]["tezaKonja"], reverse=True)
    elif sorting == "visina-":
        res.sort(key=lambda x: x["konj"]["visinaKonja"], reverse=False)
    elif sorting == "visina+":
        res.sort(key=lambda x: x["konj"]["visinaKonja"], reverse=True)
    
    html = render_to_string('konj/search_results.html', {"res": res})
    return HttpResponse(html)

    #return JsonResponse(res, safe=False)

def info(request):
    ponudbaID = request.GET.get('id', '')
    ponudba = Ponudba.objects.get(ponudbaID=ponudbaID)
    res = {}

    res["ponudba"] = {}
    res["ponudba"]["ponudbaID"] = ponudba.ponudbaID
    res["ponudba"]["cena"] = ponudba.cena

    res["konj"] = {}
    res["konj"]["imeKonja"] = ponudba.konjID.imeKonja
    res["konj"]["opisKonja"] = ponudba.konjID.opisKonja
    res["konj"]["starostKonja"] = ponudba.konjID.starostKonja
    res["konj"]["tezaKonja"] = ponudba.konjID.tezaKonja
    res["konj"]["visinaKonja"] = ponudba.konjID.visinaKonja
    res["konj"]["poskodbe"] = ponudba.konjID.poskodbe
    res["konj"]["bolezni"] = ponudba.konjID.bolezni
    res["konj"]["rezultati"] = ponudba.konjID.rezultati
    res["konj"]["linkOdKonjeniskeZveze"] = ponudba.konjID.linkOdKonjeniskeZveze
    res["konj"]["je_lastnik"] = ponudba.konjID.je_lastnik

    res["konj"]["rokovanje"] = {}
    res["konj"]["rokovanje"]["imeRokovanja"] = ponudba.konjID.rokovanjeID.imeRokovanja
    res["konj"]["rokovanje"]["rokovanjeOpis"] = ponudba.konjID.rokovanjeID.rokovanjeOpis
    res["konj"]["jahljivost"] = {}
    res["konj"]["jahljivost"]["imeJahljivosti"] = ponudba.konjID.jahljivostID.imeJahljivosti
    res["konj"]["jahljivost"]["jahljivostOpis"] = ponudba.konjID.jahljivostID.jahljivostOpis
    res["konj"]["pasma"] = {}
    res["konj"]["pasma"]["imePasme"] = ponudba.konjID.pasmaID.pasmaIme
    res["konj"]["pasma"]["pasmaOpis"] = ponudba.konjID.pasmaID.pasmaOpis

    res["konj"]["imeRegije"] = ponudba.konjID.regijaID.imeRegije

    slike = SlikeKonj.objects.filter(konjID=ponudba.konjID.konjID)
    slikeUrl = []
    for slika in slike:
        slikeUrl.append(str(slika.slikaKonja.url).split("/", 1)[1])

    res["konj"]["slike"] = slikeUrl

    res["konj"]["rodovnik"] = {}
    rodovnik = Rodovnik.objects.filter(konjID=ponudba.konjID.konjID).first()
    if rodovnik:
        res["konj"]["rodovnik"]["imaRodovnik"] = rodovnik.rodovnik_bools
        res["konj"]["rodovnik"]["slikaRodovnika"] = str(rodovnik.rodovnik_slika.url).split("/", 1)[1]

    res["konj"]["kategorije"] = []
    kategorije = SpadaVKategorijo.objects.filter(konjID=ponudba.konjID.konjID)
    for kategorija in kategorije:
        kategorijaRow = {}
        kategorijaRow["imeKategorije"] = kategorija.kategorijaID.imeKategorije
        kategorijaRow["opisKategorije"] = kategorija.kategorijaID.opisKategorije
        res["konj"]["kategorije"].append(kategorijaRow)

    res["oseba"] = {}
    res["oseba"]["imeOsebe"] = ponudba.osebaID.imeOsebe
    res["oseba"]["priimekOsebe"] = ponudba.osebaID.priimekOsebe
    res["oseba"]["ePostniNaslov"] = ponudba.osebaID.ePostniNaslov
    res["oseba"]["telefonskaStevilka"] = ponudba.osebaID.telefonskaStevilka
    res["oseba"]["telefonskaStevilka"] = ponudba.osebaID.telefonskaStevilka

    res["oseba"]["naslov"] = {}
    res["oseba"]["naslov"]["ulica"] = ponudba.osebaID.naslovID.ulica
    res["oseba"]["naslov"]["hisnaStevilka"] = ponudba.osebaID.naslovID.hisnaStevilka
    res["oseba"]["naslov"]["imeKraja"] = ponudba.osebaID.naslovID.postnaStevilka.imeKraja
    res["oseba"]["naslov"]["imeRegije"] = ponudba.osebaID.naslovID.postnaStevilka.regijaID.imeRegije

    ID_Uporabnik = request.session.get('id', -1)
    # preveri sejo
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return render(request, 'konj/info.html', {
            'ponudba': res
        })

    return render(request, 'konj/uporabnik_info.html', {
        'ponudba': res
    })

@ensure_csrf_cookie
def registration(request):
    return render(request, 'konj/registration.html')

# ajax klic za registracijo
def register(request):
    Ime = request.POST.get('Ime', '')
    Priimek = request.POST.get('Priimek', '')
    UpIme = request.POST.get('upime', '')
    opis = request.POST.get('opis','')
    email = request.POST.get('email', '')
    tel = request.POST.get('tel','')
    geslo = request.POST.get('geslo', '')


    # preveri parametre
    if len(Ime) == 0 or len(Priimek) == 0 or len(UpIme) == 0 or len(email) == 0 or len(geslo) == 0:
        return HttpResponse("1")

    # preveri ali mentor z istim up. imenom že obstaja
    if (Oseba.objects.filter(uporabniskoIme=UpIme).exists()):
        return HttpResponse("3")

    # hash = pbkdf2_sha256.encrypt(geslo, rounds=500, salt_size=16)
    hash = pbkdf2_sha256.encrypt(geslo)

    # shrani mentorja in solo v kateri uci
    # pss = make_password(geslo, salt=None, hasher='default')

    regija, created = Regija.objects.get_or_create(
        imeRegije="testRegija"
    )

    kraj, created = Kraj.objects.get_or_create(
        regijaID=regija,
        imeKraja="testKraj"
    )

    naslov, created = Naslov.objects.get_or_create(
        postnaStevilka=kraj,
        ulica="testUlica",
        hisnaStevilka=1
    )

    uporabnik = Oseba(
        imeOsebe=Ime,
        priimekOsebe=Priimek,
        uporabniskoIme=UpIme,
        ePostniNaslov=email,
        telefonskaStevilka=tel,
        geslo=hash,
        opisOsebe=opis,
        naslovID=naslov
    )
    uporabnik.save()

    #tmp = Oseba.objects.latest('osebaID')

    return HttpResponse("0")


def login(request):
    if request.session.get('id', None) != None:
        del request.session['id']

    return render(request, 'konj/login.html')


# tukaj dobimo ajax klic iz login page. Vrne ali je prijava uspešna ali ne
# err codes:
# 1 username or password not set
# 0 username and pss is correct
# 2 password error
# 3 mentor don't exists
def auth(request):
    usr = request.POST.get('usr', '')
    pss = request.POST.get('pss', '')

    if usr == '' or pss == '':
        return HttpResponse("1")

    # če najdeš zadetek vzemi geslo
    uporabnik = Oseba.objects.filter(uporabniskoIme=usr).values()

    if uporabnik:
        if pbkdf2_sha256.verify(pss, uporabnik.first()["geslo"]):
            request.session['id'] = uporabnik.first()["osebaID"]
            return HttpResponse("0")
        else:
            return HttpResponse("2")
    else:
        return HttpResponse("3")

def uporabnik_index(request):
    ID_Uporabnik = request.session.get('id', -1)
    # preveri sejo
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    ponudbe = Ponudba.objects.filter(osebaID=ID_Uporabnik)

    res = []
    for ponudba in ponudbe:
        row = {}

        row["konj"] = {}
        row["konjID"] = ponudba.konjID.konjID
        row["konj"]["imeKonja"] = ponudba.konjID.imeKonja
        row["konj"]["starostKonja"] = ponudba.konjID.starostKonja
        if (row["konj"]["starostKonja"] == None):
            row["konj"]["starostKonja"] = -1
        row["konj"]["tezaKonja"] = ponudba.konjID.tezaKonja
        if (row["konj"]["tezaKonja"] == None):
            row["konj"]["tezaKonja"] = -1
        row["konj"]["visinaKonja"] = ponudba.konjID.visinaKonja
        if (row["konj"]["visinaKonja"] == None):
            row["konj"]["visinaKonja"] = -1

        kategorije = SpadaVKategorijo.objects.filter(konjID=ponudba.konjID.konjID)
        row["konj"]["kategorije"] = []
        for kategorija in kategorije:
            kategorijaRow = {}
            kategorijaRow["imeKategorije"] = kategorija.kategorijaID.imeKategorije
            kategorijaRow["opisKategorije"] = kategorija.kategorijaID.opisKategorije
            row["konj"]["kategorije"].append(kategorijaRow)

        row["ponudba"] = {}
        row["ponudba"]["ponudbaID"] = ponudba.ponudbaID
        row["ponudba"]["cena"] = ponudba.cena
        if (row["ponudba"]["cena"] == None):
            row["ponudba"]["cena"] = -1

        row["oseba"] = {}
        row["oseba"]["imeOsebe"] = ponudba.osebaID.imeOsebe
        row["oseba"]["priimekOsebe"] = ponudba.osebaID.priimekOsebe

        slika = SlikeKonj.objects.filter(konjID=ponudba.konjID.konjID).first()
        if slika:
            row["konj"]["slika"] = str(slika.slikaKonja.url).split("/", 1)[1]
        else:
            row["konj"]["slika"] = None

        row["konj"]["rokovanje"] = {}
        row["konj"]["rokovanje"]["imeRokovanja"] = ponudba.konjID.rokovanjeID.imeRokovanja
        row["konj"]["rokovanje"]["rokovanjeOpis"] = ponudba.konjID.rokovanjeID.rokovanjeOpis
        row["konj"]["jahljivost"] = {}
        row["konj"]["jahljivost"]["imeJahljivosti"] = ponudba.konjID.jahljivostID.imeJahljivosti
        row["konj"]["jahljivost"]["jahljivostOpis"] = ponudba.konjID.jahljivostID.jahljivostOpis
        row["konj"]["pasma"] = {}
        row["konj"]["pasma"]["imePasme"] = ponudba.konjID.pasmaID.pasmaIme
        row["konj"]["pasma"]["pasmaOpis"] = ponudba.konjID.pasmaID.pasmaOpis
        row["konj"]["regija"] = {}
        row["konj"]["regija"]["imeRegije"] = ponudba.konjID.regijaID.imeRegije

        res.append(row)
    
    """
    
    konjiId = []
    for ponudba in ponudbe:
        konjiId.append(ponudba['konjID_id'])
    
    konji = Konj.objects.filter(konjID__in=konjiId).values()

    sez_last = []
    sez_jah = []
    sez_pasem = []
    sez_regij = []
    """
    """
    for konj in konji:
        sez_pasem.append(Pasma.objects.filter(pasmaID=konj["pasmaID"]).values().first()["pasmaIme"])
        konj["jahljivostID"] = Jahljivost.objects.filter(jahljivostID=konj["jahljivostID"]).values().first()["jahljivost_bool"]
        konj["rokljivost"] = Rokovanje.objects.filter(ID_Rokovanje=konj["rokovanjeID_id"]).values().first()["rokovanje_bool"]
        sez_regij.append(Regija.objects.filter(ID_Regija=konj["regijaID_id"]).values().first()["ime_regija"])
    """
    context = {'konji':res}

    return render(request, 'konj/uporabnik_index.html',context)


def profil(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")



    oseba = Oseba.objects.filter(osebaID=ID_Uporabnik).values()


    context = {'opis': oseba.first()["opisOsebe"], 'Ime': oseba.first()["imeOsebe"], 'Priimek': oseba.first()["priimekOsebe"],
               'UpIme': oseba.first()["uporabniskoIme"], 'mail': oseba.first()["ePostniNaslov"]}

    return render(request, 'konj/profil.html', context)


def profil_update(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")


    oseba = Oseba.objects.filter(osebaID=ID_Uporabnik).values()

    Ime = request.POST.get('Ime', '')
    Priimek = request.POST.get('Priimek', '')
    UpIme = request.POST.get('UpIme', '')
    email = request.POST.get('email', '')
    opis = request.POST.get('opis', '')
    geslo = request.POST.get('geslo', '')
    trenutno_geslo = request.POST.get('trenutno_geslo', '')
    id_Sola = ''

    if len(trenutno_geslo) == 0:
        return HttpResponse("5")
    # preveri ali je bilo vnešeno geslo
    if not pbkdf2_sha256.verify(trenutno_geslo, oseba.first()["geslo"]):
        return HttpResponse("6")

    # preveri ali mentor z istim up. imenom že obstaja
    if (oseba.first()["uporabniskoIme"] != UpIme and Oseba.objects.filter(uporabniskoIme=UpIme).exists()):
        return HttpResponse("3")

    if (len(Ime) > 0 and oseba.first()["imeOsebe"] != Ime):
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(imeOsebe=Ime)
    if (len(Priimek) > 0 and oseba.first()["priimekOsebe"] != Priimek):
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(priimekOsebe=Priimek)
    if (len(UpIme) > 0 and oseba.first()["uporabniskoIme"] != UpIme):
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(uporabniskoIme=UpIme)
    if (len(email) > 0 and oseba.first()["ePostniNaslov"] != email):
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(ePostniNaslov=email)
    if (len(geslo) > 0):
        hash = pbkdf2_sha256.encrypt(geslo)
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(geslo=hash)
    if(len(opis) > 0 and oseba.first()["opisOsebe"] != opis):
        Oseba.objects.filter(osebaID=ID_Uporabnik).update(opisOsebe=opis)

    return HttpResponse("0")


def delete_user(request):
    UpIme = request.POST.get('UpIme', '')
    trenutno_geslo = request.POST.get('trenutno_geslo', '')

    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")


    uporabnik=Oseba.objects.filter(upime=UpIme).values()


    #preveri vnešenost gesla
    if len(trenutno_geslo) == 0:
        return HttpResponse("2")
    #preveri ali je bilo vnešeno geslo
    if not pbkdf2_sha256.verify(trenutno_geslo, uporabnik.first()["geslo"]):
        return HttpResponse("3")


    # najprej preveri ali je pravi, varnost
    tmp=Oseba.objects.filter(upime=UpIme).values()

    if ID_Uporabnik != tmp.first()["osebaID"]:
        return HttpResponse("1")

    #če ima še dodane oglase jih mora najprej odstraniti
    if Konj.objects.filter(prodajalecID_id=ID_Uporabnik).count() > 0:
        return HttpResponse("4")

    #začni z brisanjem mentorja
   #je_lastnik.objects.filter(prodajalecID=ID_Uporabnik).delete()
    if Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).count() > 0:
        slika = Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).values().first()
        os.remove(slika["profilna_slika"])
        Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).delete()

    Oseba.objects.filter(osebaID=ID_Uporabnik).delete()


    return HttpResponse("0")

def profilna_slika(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    oseba = Oseba.objects.filter(osebaID=ID_Uporabnik).values()
    slika = Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).values()
    profilna_slika = "media/oseba/no-img.jpg"
    if Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).count() > 0:
        tmp1 = slika.first()["profilna_slika"].split("/")[-1]
        tmp2 = slika.first()["profilna_slika"].split("/")[-2]
        profilna_slika = "media/"+tmp2 + "/" + tmp1

    print(profilna_slika)
    context = {'ID_Uporabnik': ID_Uporabnik, 'profilna_slika': profilna_slika}

    return render(request, 'konj/dodaj_slika.html', context)

class ImageUploadForm(forms.Form):
    """Image upload form."""
    image = forms.ImageField()


def upload(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            if Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).count() > 0:
                slika = Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).values().first()
                os.remove(slika["profilna_slika"])
                m = Profilna_slika.objects.filter(ID_Prodajalca=ID_Uporabnik).delete()

            m = Profilna_slika.objects.create(ID_Prodajalca = Oseba.objects.get(osebaID = ID_Uporabnik))
            #m.ID_Prodajalca = ID_Uporabnik
            m.profilna_slika = form.cleaned_data['image']
            m.save()
            #tmp = 'konj/static/media/oseba/'+str(form.cleaned_data['image'])
            #Oseba.objects.filter(osebaID=ID_Uporabnik).update(profilna_slika=form.cleaned_data['image'])

            red='../profilna_slika/'
            return redirect(red)
    return HttpResponse("Izbrati morate sliko. Nobena slika ni bila dodana.")

def dodaj_konja(request):

    pasma = Pasma.objects.all().values()
    pregledi = VrstaPregleda.objects.all().values()
    kategorije = Kategorija.objects.all().values()
    jahljivost = Jahljivost.objects.all().values()
    rokovanje = Rokovanje.objects.all().values()
    regije = Regija.objects.all().values()
    context = {'pasma':pasma,'pregledi':pregledi,'kategorije':kategorije,'regije':regije, 'jahljivost': jahljivost, 'rokovanje': rokovanje}
    return render(request, 'konj/dodaj_konja.html',context)

def klic_dodaj_konja(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    konj_ime = request.POST.get('konj_ime', '')
    pasmaID = request.POST.get('pasmaID', '')
    starost = request.POST.get('starost','')
    visina = request.POST.get('visina', '')
    rodovnik_bool = request.POST.get('rodovnik_bool', '')
    jahljivost = request.POST.get('jahljivost', '')
    bolezni = request.POST.get('bolezni', '')
    rokovanje = request.POST.get('rokovanje', '')
    konj_opis = request.POST.get('konj_opis', '')
    poskodbe = request.POST.get('poskodbe', '')
    rezultati = request.POST.get('rezultati', '')
    link_konjeniska_zveza = request.POST.get('link_konjeniska_zveza', '')
    regijaID = request.POST.get('regijaID', '')
    slika_rodovnik = request.POST.get('slika_rodovnik','')
    lastnik = request.POST.get('lastnik','')
    pregledi = request.POST.get('pregledi','')
    kategorije = request.POST.get('kategorije', '')
    cena = request.POST.get('cena','')

    if(len(konj_ime) == 0 or int(starost) > 100 or starost == 0 or len(visina) == 0 or rodovnik_bool == -1 or jahljivost == -1 or rokovanje == -1 ):
        return HttpResponse("1/-1")
    regija = Regija.objects.filter(regijaID=regijaID).first()
    regija, created = Regija.objects.get_or_create(
        imeRegije="testRegija"
    )

    kraj, created = Kraj.objects.get_or_create(
        regijaID=regija,
        imeKraja="testKraj"
    )

    pasma = Pasma.objects.get(
        pasmaID=pasmaID
    )

    rokovanje = Rokovanje.objects.get(
        rokovanjeID=rokovanje
    )

    jahljivost = Jahljivost.objects.get(
        jahljivostID=jahljivost
    )

    #konj = Konj(None,starost,regijaID,visina,ID_Jahljivost.ID_Jahljivost,ID_Uporabnik,pasmaID,ID_Rokovanje.ID_Rokovanje,konj_opis,konj_ime,poskodbe,bolezni,rezultati,link_konjeniska_zveza,lastnik)
    konj = Konj(
        pasmaID=pasma,
        rokovanjeID=rokovanje,
        jahljivostID=jahljivost,
        regijaID=regija,
        opisKonja=konj_opis,
        poskodbe=poskodbe,
        bolezni=bolezni,
        rezultati=rezultati,
        linkOdKonjeniskeZveze=link_konjeniska_zveza,
        imeKonja=konj_ime,
        je_lastnik=lastnik,
        starostKonja=starost,
        visinaKonja=visina
    )
    konj.save()
    tmp = Konj.objects.latest('konjID')
    konjID = tmp.konjID

    #dodajanje kategorije konju
    seznam_kategorij = kategorije.split("/")
    seznam_kategorij.pop()
    for kategorija in seznam_kategorij:
        tmp = SpadaVKategorijo(None,konjID,kategorija)
        tmp.save()

    #dodajanje pregledov konju
    seznam_pregledov = pregledi.split("/")
    seznam_pregledov.pop()
    for pregled in seznam_pregledov:
        tmp = VeterinarskiPregledi(None,konjID,pregled)
        tmp.save()


    oseba = Oseba.objects.get(
        osebaID=ID_Uporabnik
    )

    ponudba = Ponudba(
        osebaID=oseba,
        konjID=konj,
        cena=cena,
        status=1
    )
    ponudba.save()

    response='0/'+str(konjID)

    return HttpResponse(response)


def dodaj_slika_konj(request,id):
    ID_Uporabnik = request.session.get('id', -1)
    ID_Konja = id

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    #seznam povezav na slike
    sez_slik = []

    #seznam id-jev teh slik
    sez_id_slik = []

    slike = SlikeKonj.objects.filter(konjID=ID_Konja).values()
    for s in slike:
        tmp1 = s["slikaKonja"].split("/")[-1]
        tmp2 = s["slikaKonja"].split("/")[-2]
        tmp3 = "media/" + tmp2 + "/" + tmp1
        sez_id_slik.append(s["slikaID"])
        sez_slik.append(tmp3)

    context = {'ID_Uporabnik': ID_Uporabnik, 'sez_slik':zip(sez_slik,sez_id_slik),'ID_Konja':ID_Konja}

    return render(request, 'konj/dodaj_slika_konj.html', context)

class ImageUploadForm(forms.Form):
    """Image upload form."""
    image = forms.ImageField()

def upload_konj(request,id):
    ID_Uporabnik = request.session.get('id', -1)
    ID_Konja=id
    konj=Konj.objects.filter(konjID=ID_Konja).first()
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    if(max_st_slik < SlikeKonj.objects.filter(konjID=ID_Konja).count()+1):
        return HttpResponse("Presegli ste največje število slik "+str(max_st_slik)+" prosim izbrišite kakšno in potem dodajajte novo")


    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            slika_konj = SlikeKonj.objects.create(slikaID=None,konjID=konj,slikaKonja=form.cleaned_data['image'])

            red='../dodaj_slika_konj/'+str(id)
            return redirect(red)
    return HttpResponse("Izbrati morate sliko. Nobena slika ni bila dodana.")

def klic_izbrisi_sliko_konja(request):
    ID_Uporabnik = request.session.get('id', -1)
    ID_Slika = request.POST.get('idslika', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")


    if(ID_Slika == -1):
        return HttpResponse("Slika ni bila izbrisana")

    slika = SlikeKonj.objects.filter(slikaID=ID_Slika).values().first()
    os.remove(slika["slikaKonja"])
    SlikeKonj.objects.filter(slikaID=ID_Slika).delete()


    return HttpResponse("0")

def spremeni_konja(request,id):
    ID_Uporabnik = request.session.get('id', -1)
    ID_Konja = id
    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    konj = Konj.objects.filter(konjID=ID_Konja).values().first()
    kategorije = Kategorija.objects.all().values()
    pasma = Pasma.objects.all().values()
    regije = Regija.objects.all().values()
    pregledi = VrstaPregleda.objects.all().values()
    rokovanje = Rokovanje.objects.all().values()
    tmp = VeterinarskiPregledi.objects.all().values()

    trenutna_pasma=Pasma.objects.filter(pasmaID=konj["pasmaID_id"]).values().first()
    trenutna_regija=Regija.objects.filter(regijaID=konj["regijaID_id"]).values().first()
    jahljivost=Jahljivost.objects.filter(jahljivostID=konj["jahljivostID_id"]).values().first()
    rokovanje=Rokovanje.objects.filter(rokovanjeID=konj["rokovanjeID_id"]).values().first()
    ponudba = Ponudba.objects.filter(konjID_id=ID_Konja,osebaID_id=ID_Uporabnik).values().first()

    sez_kategorije=[]
    spada_v_kategorijo_sez = []
    for kategorija in kategorije:
        sez_kategorije.append(kategorija)
        if SpadaVKategorijo.objects.filter(kategorijaID=kategorija["kategorijaID"],konjID=ID_Konja).exists():
            spada_v_kategorijo_sez.append(1)
        else:
            spada_v_kategorijo_sez.append(0)

    #dodaj seznam ki pobr ali konj spada pod pregled ali ne
    #1->spada
    #0->ne spada
    sez_pregledi = []
    spada_v_pregled_sez = []
    for pregled in pregledi:
        sez_pregledi.append(pregled)
        if VeterinarskiPregledi.objects.filter(vrstaPregledaID=pregled["vrstaPregledaID"],konjID_id=ID_Konja).exists():
            spada_v_pregled_sez.append(1)
        else:
            spada_v_pregled_sez.append(0)


    context = {'konj':konj,'ponudba':ponudba,'kategorije':zip(sez_kategorije,spada_v_kategorijo_sez),'pregledi':zip(sez_pregledi,spada_v_pregled_sez),'pasma':pasma,'regije':regije,'trenutna_pasma':trenutna_pasma,'trenutna_regija':trenutna_regija,'jahljivost':jahljivost,'rokovanje':rokovanje}
    return render(request, 'konj/spremeni_konja.html',context)

def klic_spremeni_konja(request):
    ID_Uporabnik = request.session.get('id', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    ID_Konj= request.POST.get('ID_Konj', '')
    konj_ime = request.POST.get('konj_ime', '')
    pasmaID = request.POST.get('pasmaID', '')
    lastnik = request.POST.get('lastnik', '')
    starost = request.POST.get('starost', '')
    visina = request.POST.get('visina', '')
    #rodovnik_bool = request.POST.get('rodovnik_bool', '')
    jahljivost_bool = request.POST.get('jahljivost_bool', '')
    jahljivost_opis = request.POST.get('jahljivost_opis', '')
    bolezni = request.POST.get('bolezni', '')
    rokovanje_bool = request.POST.get('rokovanje_bool', '')
    rokovanje_opis = request.POST.get('rokovanje_opis', '')
    konj_opis = request.POST.get('konj_opis', '')
    poskodbe = request.POST.get('poskodbe', '')
    rezultati = request.POST.get('rezultati', '')
    link_konjeniska_zveza = request.POST.get('link_konjeniska_zveza', '')
    regijaID = request.POST.get('regijaID', '')
    pregledi = request.POST.get('pregledi', '')
    kategorije = request.POST.get('kategorije', '')
    cena = request.POST.get('cena','')

    konj=Konj.objects.filter(konjID=ID_Konj).values().first()


    if (len(ID_Konj) == 0 or len(konj_ime) == 0 or int(starost) > 100 or lastnik == -1 or starost == 0 or len(visina) == 0 or jahljivost_bool == -1 or rokovanje_bool == -1 or len(konj_opis) == 0):
        return HttpResponse("1")
#jahjlkivost,rokovanje,regija
    Konj.objects.filter(konjID=ID_Konj).update(opisKonja=konj_opis,imeKonja=konj_ime,poskodbe=poskodbe,bolezni=bolezni,rezultati=rezultati,linkOdKonjeniskeZveze=link_konjeniska_zveza, je_lastnik=lastnik,pasmaID_id=pasmaID,regijaID_id=regijaID,starostKonja=starost,visinaKonja=visina)
    Jahljivost.objects.filter(jahljivostID=konj["jahljivostID_id"]).update(imeJahljivosti=jahljivost_bool,jahljivostOpis=jahljivost_opis)
    Rokovanje.objects.filter(rokovanjeID=konj["rokovanjeID_id"]).update(imeRokovanja=rokovanje_bool,rokovanjeOpis=rokovanje_opis)
    Ponudba.objects.filter(konjID_id=ID_Konj,osebaID_id=ID_Uporabnik).update(cena=cena)
    # dodajanje pregledov za konja

    #najprej vse zbriši nato mu dodajaj
    VeterinarskiPregledi.objects.filter(konjID_id=ID_Konj).delete()
    seznam_pregledov = pregledi.split("/")
    seznam_pregledov.pop()
    for pregled in seznam_pregledov:
        tmp = VeterinarskiPregledi(None, ID_Konj, pregled)
        tmp.save()

    # dodajanje kategorije konju
    SpadaVKategorijo.objects.filter(konjID_id=ID_Konj).delete()

    seznam_kategorij = kategorije.split("/")

    seznam_kategorij.pop()
    for kategorija in seznam_kategorij:
        tmp = SpadaVKategorijo(None,ID_Konj, kategorija)
        tmp.save()


    return HttpResponse("0")

def izbrisi_konja(request):

    ID_Uporabnik = request.session.get('id', -1)
    ID_Konj = request.POST.get('ID_Konj', -1)

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")

    if ID_Konj == -1:
        return HttpResponse("Noben konj ni bil izbran")

    SpadaVKategorijo.objects.filter(konjID_id=ID_Konj).delete()
    VeterinarskiPregledi.objects.filter(konjID_id=ID_Konj).delete()


    #izbriši vse slike ki so spadale pod konja
    if SlikeKonj.objects.filter(konjID_id=ID_Konj).count() > 0:
        slike_konj=SlikeKonj.objects.filter(konjID_id=ID_Konj).values()
        for slika in slike_konj:
            os.remove(slika["slikaKonja"])
        SlikeKonj.objects.filter(konjID_id=ID_Konj).delete()

    if Rodovnik.objects.filter(konjID_id=ID_Konj).count() > 0:
        os.remove(Rodovnik.objects.filter(konjID_id=ID_Konj).values().first()["rodovnik_slika"])
        Rodovnik.objects.filter(konjID_id=ID_Konj).delete()
    print(Konj.objects.filter(konjID=ID_Konj).values().first())
    Ponudba.objects.filter(konjID_id=ID_Konj).delete()
    Konj.objects.filter(konjID=ID_Konj).delete()

    return HttpResponse("0")


#dodaj slika rodovnika
def dodaj_slika(request):
    ID_Uporabnik = request.session.get('id', -1)
    image = request.FILES['image']
    ID_Konj = request.POST.get('ID_Konj','')

    if not Oseba.objects.filter(osebaID=ID_Uporabnik).exists() or ID_Uporabnik == -1:
        return HttpResponse("Nimate veljavne seje, prosim prijavite se se enkrat")
    slika_konj = Rodovnik.objects.create(ID_Rodovnik=None, konjID_id=ID_Konj, rodovnik_slika=image,rodovnik_bools="1")

    return HttpResponse("0")
