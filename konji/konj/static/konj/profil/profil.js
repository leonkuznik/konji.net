function reg() {
    alert("dela")
    //spr pove ali naj se izvede ajax klic ali ne
    let err = false;
    //pridobi podatke
    let Ime=document.getElementById("Ime").value;
    let Priimek=document.getElementById("Priimek").value;
    let UpIme=document.getElementById("UpIme").value;
    let email=document.getElementById("email").value;
    let opis=document.getElementById("opis").value;
    let geslo=document.getElementById("geslo").value;
    let geslo_ponovno=document.getElementById("geslo_ponovno").value;
    let trenutno_geslo=document.getElementById("trenutno_geslo").value;

    let status=document.getElementById("status");

    if(geslo != geslo_ponovno)
    {
        status.innerHTML="Gesli se ne ujemata";
        status.style.color="red";
        err = true;
    }

    if(err == false)
    {
        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });


        //ajax klic za spremembo podatkov
        $.ajax({
                type:"POST",
                url:"profil_update/",
                data: {
                    'Ime': Ime,
                    'Priimek': Priimek,
                    'UpIme': UpIme,
                    'email': email,
                    'opis': opis,
                    'geslo': geslo,
                    'trenutno_geslo': trenutno_geslo,
                },
                success: function(data){
                    alert(data)



                    if(data == '0'){
                        status.innerHTML="Sprememba podatkov uspešna";
                        status.style.color="green";
                    }
                    if(data == '1')
                    {
                        status.innerHTML="Težava pri procesiranju parametrov";
                        status.style.color="red";
                    }
                    if(data == '2')
                    {
                        status.innerHTML="Šole ni v sistemu, preverite črkovanje.";
                        status.style.color="red";
                    }
                    if(data == '3')
                    {
                        status.innerHTML="Uporabniško ime je že zasedeno.";
                        status.style.color="red";
                    }
                    if(data == '4')
                    {
                        alert("Ker ste spreminili šolo se boste morali še enkrat prijaviti");
                        location.href="../login";
                    }
                    if(data == '5')
                    {
                        status.innerHTML="Za spremembo podatkov morate vnesti svoje trenutno geslo s katerim se prijavite.";
                        status.style.color="red";
                    }
                    if(data == '6')
                    {
                        status.innerHTML="Vnesli ste nepravilno geslo. Vnesti morate tisto s katerim ste se nazadnje prijavili ali spremenili.";
                        status.style.color="red";
                    }


                }
        });
        
    }
    }




function del(){

    alert("dela")
    let UpIme=document.getElementById("UpIme").value;
    let trenutno_geslo=document.getElementById("trenutno_geslo").value;

    let status=document.getElementById("status");



        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });


        //ajax klic za spremembo podatkov
        $.ajax({
                type:"POST",
                url:"delete_user/",
                data: {
                    'UpIme': UpIme,
                    'trenutno_geslo': trenutno_geslo,
                },
                success: function(data){

                    if(data == '0')
                    {
                        alert("Uspešno ste se izbrisali.");
                        location.href="login";
                    }
                    if(data == '1')
                    {
                        status.innerHTML="Nekaj je šlo narobe"
                        status.style.color="red";
                    }
                    if(data == '2')
                    {
                        status.innerHTML="Za spremembo podatkov morate vnesti svoje trenutno geslo s katerim se prijavite.";
                        status.style.color="red";
                    }
                    if(data == '3')
                    {
                        status.innerHTML="Vnesli ste nepravilno geslo. Vnesti morate tisto s katerim ste se nazadnje prijavili ali spremenili.";
                        status.style.color="red";
                    }
                    if(data == '4')
                    {
                        status.innerHTML="Če se želite izbrisati morate najprej izbrisati vse svoje oglase.";
                        status.style.color="red";
                    }


                }
        }); }




