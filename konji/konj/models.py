# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Jahljivost(models.Model):
    jahljivostID = models.AutoField(db_column='jahljivostID', primary_key=True)
    imeJahljivosti = models.CharField(db_column='imeJahljivosti', max_length=20)
    jahljivostOpis = models.CharField(db_column='jahljivostOpis', max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'Jahljivost'


class Kategorija(models.Model):
    kategorijaID = models.AutoField(db_column='kategorijaID', primary_key=True)
    imeKategorije = models.CharField(db_column='imeKategorije', max_length=30)
    opisKategorije = models.CharField(db_column='opisKategorije', max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'Kategorija'


class Konj(models.Model):
    konjID = models.AutoField(db_column='konjID', primary_key=True)
    pasmaID = models.ForeignKey('Pasma', db_column='pasmaID', on_delete=None)
    rokovanjeID = models.ForeignKey('Rokovanje', db_column='rokovanjeID', on_delete=None)
    #naslovID = models.ForeignKey('Naslov', db_column='naslovID', on_delete=None, related_name="KonjNaslovID")
    jahljivostID = models.ForeignKey('Jahljivost', db_column='jahljivostID', on_delete=None)
    regijaID = models.ForeignKey('Regija', db_column='regijaID', on_delete=None)
    opisKonja = models.CharField(db_column='opisKonja', max_length=300, blank=True, null=True)
    poskodbe = models.CharField(db_column='poskodbe', max_length=1024, blank=True, null=True)
    bolezni = models.CharField(db_column='bolezni', max_length=1024, blank=True, null=True)
    rezultati = models.CharField(db_column='rezultati', max_length=1024, blank=True, null=True)
    linkOdKonjeniskeZveze = models.CharField(db_column='linkOdKonjeniskeZveze', max_length=200, blank=True, null=True)
    imeKonja = models.CharField(db_column='imeKonja', max_length=30)
    starostKonja = models.IntegerField(db_column='starostKonja', blank=True, null=True)
    tezaKonja = models.IntegerField(db_column='tezaKonja', blank=True, null=True)
    visinaKonja = models.IntegerField(db_column='visinaKonja', blank=True, null=True)
    #rodovnikSlika = models.ImageField(db_column='rodovnikSlika', blank=True, null=True, upload_to='konj/static/media/rodovnik/', default='konj/static/media/rodovnik/no-img.jpg')
    je_lastnik = models.IntegerField(db_column='je_lastnik',blank=True, null=True)
    class Meta:
        db_table = 'Konj'


class Rodovnik(models.Model):
    ID_Rodovnik = models.AutoField(primary_key=True)
    konjID=models.ForeignKey(Konj,on_delete=None,related_name='rodovnik')
    rodovnik_slika = models.ImageField(upload_to = 'konj/static/media/rodovnik/', default = 'konj/static/media/rodovnik/no-img.jpg')
    rodovnik_bools = models.CharField(max_length=2)
    class Meta:
        db_table = 'Rodovnik'

class Kraj(models.Model):
    postnaStevilka = models.AutoField(db_column='postnaStevilka', primary_key=True)
    regijaID = models.ForeignKey('Regija', db_column='regijaID', on_delete=None)
    imeKraja = models.CharField(db_column='imeKraja', max_length=30)

    class Meta:
        db_table = 'Kraj'


class Naslov(models.Model):
    naslovID = models.AutoField(db_column='naslovID', primary_key=True)
    postnaStevilka = models.ForeignKey('Kraj', db_column='postnaStevilka', on_delete=None)
    ulica = models.CharField(db_column='ulica', max_length=30)
    hisnaStevilka = models.IntegerField(db_column='hisnaStevilka')

    class Meta:
        db_table = 'Naslov'
        unique_together = (('postnaStevilka', 'naslovID'),)


class Oseba(models.Model):
    osebaID = models.AutoField(db_column='osebaID', primary_key=True)
    imeOsebe = models.CharField(db_column='imeOsebe', max_length=30)
    priimekOsebe = models.CharField(db_column='priimekOsebe', max_length=30)
    uporabniskoIme = models.CharField(db_column='uporabniskoIme', max_length=50)
    ePostniNaslov = models.CharField(db_column='ePostniNaslov', max_length=30)
    telefonskaStevilka = models.CharField(db_column='telefonskaStevilka', max_length=10)
    geslo = models.CharField(db_column='geslo', max_length=100)
    opisOsebe = models.CharField(db_column='opisOsebe', max_length=300, blank=True, null=True)
    #profilnaSlika = models.ImageField(db_column='profilnaSlika', blank=True, null=True, upload_to='konj/static/media/oseba/', default='konj/static/media/oseba/no-img.jpg')
    naslovID = models.ForeignKey('Naslov', db_column='naslovID', on_delete=None, related_name="OsebaNaslovID")

    class Meta:
        db_table = 'Oseba'

class Profilna_slika(models.Model):
    ID_Profilna = models.AutoField(primary_key=True)
    ID_Prodajalca = models.ForeignKey(Oseba, on_delete=None,related_name='ID_Prodajalca')
    profilna_slika = models.ImageField(upload_to = 'konj/static/media/oseba/', default = 'konj/static/media/oseba/no-img.jpg')

    class Meta:
        db_table = 'Profilna_slika'

class Pasma(models.Model):
    pasmaID = models.AutoField(db_column='pasmaID', primary_key=True)
    pasmaIme = models.CharField(db_column='pasmaIme', max_length=30)
    pasmaOpis = models.CharField(db_column='pasmaOpis', max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'Pasma'


class Ponudba(models.Model):
    ponudbaID = models.AutoField(db_column='ponudbaID', primary_key=True)
    osebaID = models.ForeignKey(Oseba, db_column='osebaID', on_delete=None)
    konjID = models.ForeignKey(Konj, db_column='konjID', on_delete=None)
    cena = models.IntegerField(db_column='cena')
    datumKreiranja = models.DateField(db_column='datumKreiranja', blank=True, null=True)
    status = models.IntegerField(db_column='status')
    datumZakljucka = models.DateField(db_column='datumZakljucka', blank=True, null=True)

    class Meta:
        db_table = 'Ponudba'
        unique_together = (('ponudbaID', 'osebaID', 'konjID'),)


class Regija(models.Model):
    regijaID = models.AutoField(db_column='regijaID', primary_key=True)
    imeRegije = models.CharField(db_column='imeRegije', max_length=30)

    class Meta:
        db_table = 'Regija'


class Rokovanje(models.Model):
    rokovanjeID = models.AutoField(db_column='rokovanjeID', primary_key=True)
    imeRokovanja = models.CharField(db_column='imeRokovanja', max_length=20)
    rokovanjeOpis = models.CharField(db_column='rokovanjeOpis', max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'Rokovanje'


class SlikeKonj(models.Model):
    slikaID = models.AutoField(db_column='slikaID', primary_key=True)
    konjID = models.ForeignKey('Konj', db_column='konjID', on_delete=None)
    slikaKonja = models.ImageField(db_column='slikaKonja', upload_to='konj/static/media/konji/', default='konj/static/media/konji/no-img.jpg')

    class Meta:
        db_table = 'SlikeKonj'
        unique_together = (('konjID', 'slikaID'),)


class SpadaVKategorijo(models.Model):
    konjID = models.ForeignKey('Konj', db_column='konjID', on_delete=None)
    kategorijaID = models.ForeignKey('Kategorija', db_column='kategorijaID', on_delete=None)

    class Meta:
        db_table = 'SpadaVKategorijo'
        unique_together = (('konjID', 'kategorijaID'),)


class VeterinarskiPregledi(models.Model):
    pregledID = models.AutoField(db_column='pregledID', primary_key=True)
    konjID = models.ForeignKey('Konj', db_column='konjID', on_delete=None)
    vrstaPregledaID = models.ForeignKey('VrstaPregleda', db_column='vrstaPregledaID', on_delete=None)
    #rezultatiPregleda = models.CharField(db_column='rezultatiPregleda', max_length=300)
    #datumPregleda = models.DateField(db_column='datumPregleda', blank=True, null=True)

    class Meta:
        db_table = 'VeterinarskiPregledi'
        unique_together = (('konjID', 'vrstaPregledaID', 'pregledID'),)


class VrstaPregleda(models.Model):
    vrstaPregledaID = models.AutoField(db_column='vrstaPregledaID', primary_key=True)
    imePregleda = models.CharField(db_column='imePregleda', max_length=20)
    opisPregleda = models.CharField(db_column='opisPregleda', max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'VrstaPregleda'
