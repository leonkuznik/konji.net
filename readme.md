### Zaganjanje aplikacije:
1. Nameščeno moraš imati MySQL in ustvarjeno podatkovno bazo z imenom Konji
2. Greš v mapo Konji
3. `python manage.py makemigrations`
Lahk pride do problemov, če nimaš napeščenih par paketov. 
Za mysqlclient, če ga ne moreš namestit prek pip:
    1. Pejt na https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient in najd verzjio mysqlclienta za svoj python
    2. pol pa `pip install <pot do .whl>`
4. `python manage.py migrate`
5. `python manage.py runserver`

### Navodila za delo z git
Ko greš nekaj delat, si nared svoj branch:
1. Greš na master branch: `git checkout master`
2. Updejtaš master na najnovejšo verzijo: `git pull origin master`
3. Ustvariš svoj branch: `git checkout -b <ime tvojga brancha>`
Pol delaš na tem branchu. Ko nardiš svoje, greš na gitlab in nardiš [merge request] za merganje z master.