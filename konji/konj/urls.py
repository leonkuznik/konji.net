from django.urls import path
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin
from . import views

app_name = 'konj'

urlpatterns = [

    path('', views.home, name='home'),
    path('search', views.search, name='search'),
    path('getSearchResults', views.getSearchResults, name='getSearchResults'),
    path('info', views.info, name="info"),

    path('registration',views.registration, name='registration'),
    path('register/',views.register, name='register'),
    path('login',views.login, name='login'),
    path('auth/', views.auth, name='auth'),
    #path('uporabnik_home', views.uporabnik_home, name='uporabnik_home'),
    path('uporabnik_index', views.uporabnik_index, name='uporabnik_index'),
    path('profil', views.profil, name='profil'),
    path('profil_update/', views.profil_update, name='profil_update'),
    path('delete_user/', views.delete_user, name='delete_user'),
    path('profilna_slika/', views.profilna_slika, name='profilna_slika'),
    path('dodaj_slika', views.dodaj_slika, name='dodaj_slika'),

    path('dodaj_slika_konj/<int:id>', views.dodaj_slika_konj, name='dodaj_slika_konj'),
    path('upload/', views.upload, name='upload'),
    path('upload_konj/<int:id>', views.upload_konj, name='upload_konj'),
    path('dodaj_konja', views.dodaj_konja, name='dodaj_konja'),
    path('klic_dodaj_konja', views.klic_dodaj_konja, name='klic_dodaj_konja'),
    path('klic_izbrisi_sliko_konja',views.klic_izbrisi_sliko_konja,name='klic_izbrisi_sliko_konja'),
    path('spremeni_konja/<int:id>',views.spremeni_konja,name='spremeni_konja'),
    path('klic_spremeni_konja', views.klic_spremeni_konja, name='klic_spremeni_konja'),
    path('izbrisi_konja', views.izbrisi_konja, name='izbrisi_konja'),

]

#path('articles/<slug:title>/<int:section>/', views.section, name='article-section'),