$(document).ready(function() {
    $(".dropdown-menu").on("click", function (e) {
        e.stopPropagation();
    });
    
    var starostSlider = $("#starostSlider").slider({
        min: starostMinMin,
        max: starostMaxMax,
        step: 1,
        value: [starostMinMin, starostMaxMax]
    });

    starostSlider.on("change", function(e) {
        $("#starostMin").text(e.value.newValue[0] + " mes")
        $("#starostMax").text(e.value.newValue[1] + " mes")
    })

    $("#starostMin").text(starostSlider.slider('getValue')[0] + " mes")
    $("#starostMax").text(starostSlider.slider('getValue')[1] + " mes")

    var tezaSlider = $("#tezaSlider").slider({
        min: tezaMinMin,
        max: tezaMaxMax,
        step: 50,
        value: [tezaMinMin, tezaMaxMax]
    });

    tezaSlider.on("change", function(e) {
        $("#tezaMin").text(e.value.newValue[0] + " kg")
        $("#tezaMax").text(e.value.newValue[1] + " kg")
    })

    $("#tezaMin").text(tezaSlider.slider('getValue')[0] + " kg")
    $("#tezaMax").text(tezaSlider.slider('getValue')[1] + " kg")

    var visinaSlider = $("#visinaSlider").slider({
        min: visinaMinMin,
        max: visinaMaxMax,
        step: 10,
        value: [visinaMinMin, visinaMaxMax]
    });

    visinaSlider.on("change", function(e) {
        $("#visinaMin").text(e.value.newValue[0] + " cm")
        $("#visinaMax").text(e.value.newValue[1] + " cm")
    })

    $("#visinaMin").text(visinaSlider.slider('getValue')[0] + " cm")
    $("#visinaMax").text(visinaSlider.slider('getValue')[1] + " cm")

    $("#iskanje").on("click", function(){
        var url = "/search";
        var parameters = [];
        
        var pasmaParameters = getParamsFromDropdown("pasma");
        if (pasmaParameters) parameters.push(pasmaParameters);

        var kategorijaParameters = getParamsFromDropdown("kategorija");
        if (kategorijaParameters) parameters.push(kategorijaParameters);

        var rokovanjeParameters = getParamsFromDropdown("rokovanje");
        if (rokovanjeParameters) parameters.push(rokovanjeParameters);

        var jahljivostParameters = getParamsFromDropdown("jahljivost");
        if (jahljivostParameters) parameters.push(jahljivostParameters);

        var regijaParameters = getParamsFromDropdown("regija");
        if (regijaParameters) parameters.push(regijaParameters);

        parameters.push(getParamsFromSlider("starost", starostSlider));
        parameters.push(getParamsFromSlider("teza", tezaSlider));
        parameters.push(getParamsFromSlider("visina", visinaSlider));

        if (parameters.length > 0) {
            url += "?";
            for (var i = 0; i < parameters.length - 1; i++) {
                url += parameters[i] + "&";
            }
            url += parameters[parameters.length - 1];
        } 

        window.location.href = url;
    });
});

function getParamsFromDropdown(dropdown) {
    var string = "";
    var checked = []
    var checkboxes = $("[name='" + dropdown + "']");
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checked.push(checkboxes[i].id.split("-")[1]);
        }
    }
    if (checked.length > 0) {
        string += dropdown + "=";
        for (var i = 0; i < checked.length - 1; i++) {
            string += checked[i] + ","
        }
        string += checked[checked.length - 1];
        return string;
    }
    return null;
}

function getParamsFromSlider(name, slider) {
    var string = "";
    string += name + "Min=" + slider.slider('getValue')[0];
    string += "&" + name + "Max=" + slider.slider('getValue')[1];
    return string;
}