var starostSlider;
var tezaSlider;
var visinaSlider;
var currentRequest;

$(document).ready(function() {
    starostSlider = $("#starostSlider").slider({
        min: starostMinMin,
        max: starostMaxMax,
        step: 1,
        value: [starostMinMin, starostMaxMax]
    });

    starostSlider.on("change", function(e) {
        $("#starostMin").text(e.value.newValue[0] + " mes")
        $("#starostMax").text(e.value.newValue[1] + " mes")
    })

    starostSlider.slider("setValue", [starostMin, starostMax], true);

    $("#starostMin").text(starostSlider.slider('getValue')[0] + " mes")
    $("#starostMax").text(starostSlider.slider('getValue')[1] + " mes")

    tezaSlider = $("#tezaSlider").slider({
        min: tezaMinMin,
        max: tezaMaxMax,
        step: 50,
        value: [tezaMinMin, tezaMaxMax]
    });

    tezaSlider.on("change", function(e) {
        $("#tezaMin").text(e.value.newValue[0] + " kg")
        $("#tezaMax").text(e.value.newValue[1] + " kg")
    })

    tezaSlider.slider("setValue", [tezaMin, tezaMax], true);

    $("#tezaMin").text(tezaSlider.slider('getValue')[0] + " kg")
    $("#tezaMax").text(tezaSlider.slider('getValue')[1] + " kg")

    visinaSlider = $("#visinaSlider").slider({
        min: visinaMinMin,
        max: visinaMaxMax,
        step: 10,
        value: [visinaMinMin, visinaMaxMax]
    });

    visinaSlider.on("change", function(e) {
        $("#visinaMin").text(e.value.newValue[0] + " cm")
        $("#visinaMax").text(e.value.newValue[1] + " cm")
    })

    visinaSlider.slider("setValue", [visinaMin, visinaMax], true);

    $("#visinaMin").text(visinaSlider.slider('getValue')[0] + " cm")
    $("#visinaMax").text(visinaSlider.slider('getValue')[1] + " cm")

    var pasmaCheckboxes = $("[name='pasma']");
    for(var i = 0; i < pasmaCheckboxes.length; i++) {
        var id = pasmaCheckboxes[i].id.split("-")[1]
        if (pasmaSelected.includes(id)) pasmaCheckboxes[i].checked = true;
    }

    var kategorijaCheckboxes = $("[name='kategorija']");
    for(var i = 0; i < kategorijaCheckboxes.length; i++) {
        var id = kategorijaCheckboxes[i].id.split("-")[1]
        if (kategorijaSelected.includes(id)) kategorijaCheckboxes[i].checked = true;
    }

    var rokovanjeCheckboxes = $("[name='rokovanje']");
    for(var i = 0; i < rokovanjeCheckboxes.length; i++) {
        var id = rokovanjeCheckboxes[i].id.split("-")[1]
        if (rokovanjeSelected.includes(id)) rokovanjeCheckboxes[i].checked = true;
    }

    var jahljivostCheckboxes = $("[name='jahljivost']");
    for(var i = 0; i < jahljivostCheckboxes.length; i++) {
        var id = jahljivostCheckboxes[i].id.split("-")[1]
        if (jahljivostSelected.includes(id)) jahljivostCheckboxes[i].checked = true;
    }

    var regijaCheckboxes = $("[name='regija']");
    for(var i = 0; i < regijaCheckboxes.length; i++) {
        var id = regijaCheckboxes[i].id.split("-")[1]
        if (regijaSelected.includes(id)) regijaCheckboxes[i].checked = true;
    }

    search()

    $("[name='pasma']").on("change", search);
    $("[name='kategorija']").on("change", search);
    $("[name='rokovanje']").on("change", search);
    $("[name='jahljivost']").on("change", search);
    $("[name='regija']").on("change", search);
    starostSlider.on("slideStop", search);
    tezaSlider.on("slideStop", search);
    visinaSlider.on("slideStop", search);
    $('#sorting').on("change", search);
});

function info(ponudbaID) {
    window.location.href = "info?id=" + ponudbaID;
}

function search() {
    var url = "";
    var parameters = [];
    
    var pasmaParameters = getParamsFromDropdown("pasma");
    if (pasmaParameters) parameters.push(pasmaParameters);

    var kategorijaParameters = getParamsFromDropdown("kategorija");
    if (kategorijaParameters) parameters.push(kategorijaParameters);

    var rokovanjeParameters = getParamsFromDropdown("rokovanje");
    if (rokovanjeParameters) parameters.push(rokovanjeParameters);

    var jahljivostParameters = getParamsFromDropdown("jahljivost");
    if (jahljivostParameters) parameters.push(jahljivostParameters);

    var regijaParameters = getParamsFromDropdown("regija");
    if (regijaParameters) parameters.push(regijaParameters);

    parameters.push(getParamsFromSlider("starost", starostSlider));
    parameters.push(getParamsFromSlider("teza", tezaSlider));
    parameters.push(getParamsFromSlider("visina", visinaSlider));

    if (parameters.length > 0) {
        url += "?";
        for (var i = 0; i < parameters.length - 1; i++) {
            url += parameters[i] + "&";
        }
        url += parameters[parameters.length - 1];
    }

    $("#searchResults").html("Nalaganje ponudb...");
    changeUrlButDontReload("search" + url);
    if (currentRequest) currentRequest.abort();
    currentRequest = $.get("getSearchResults" + url, {
        "sorting": $('#sorting').val()
    })
        .done(function(data) {
            if (data.length === 0) $("#searchResults").html("Ni ponudb z nastavljenimi filtri.");
            else $("#searchResults").html(data);
            /*
            for (var i = 0; i < data.length; i++) {
                $("#searchResults").append("<p>" + JSON.stringify(data[i]) + "</p>");
            }
            */
        });
}

function changeUrlButDontReload(url) {
    window.history.pushState('page', $("#searchResults").pageTitle, url);
}

function getParamsFromDropdown(dropdown) {
    var string = "";
    var checked = []
    var checkboxes = $("[name='" + dropdown + "']");
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checked.push(checkboxes[i].id.split("-")[1]);
        }
    }
    if (checked.length > 0) {
        string += dropdown + "=";
        for (var i = 0; i < checked.length - 1; i++) {
            string += checked[i] + ","
        }
        string += checked[checked.length - 1];
        return string;
    }
    return null;
}

function getParamsFromSlider(name, slider) {
    var string = "";
    string += name + "Min=" + slider.slider('getValue')[0];
    string += "&" + name + "Max=" + slider.slider('getValue')[1];
    return string;
}