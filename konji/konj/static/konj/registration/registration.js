function reg() {
    //spr pove ali naj se izvede ajax klic ali ne
    let err = false;

    //pridobi podatke
    let Ime=document.getElementById("Ime").value;
    let Priimek=document.getElementById("Priimek").value;
    let opis=document.getElementById("opis").value;
    let upime=document.getElementById("upime").value;
    let email=document.getElementById("email").value;
    let tel=document.getElementById("tel").value;
    let geslo=document.getElementById("geslo").value;
    let geslo_ponovno=document.getElementById("geslo_ponovno").value;
    let status=document.getElementById("status");

    if(Ime.length == 0 || upime.length == 0 ||Priimek.length == 0 || opis.length == 0 || email.length == 0 || geslo.length == 0 || geslo_ponovno.length == 0)
    {
        alert("Izpolni vsa polja")
        err = true;
    }

    if(geslo != geslo_ponovno)
    {
        status.innerHTML="Gesli se ne ujemata";
        status.style.color="red";
        err = true;
    }

    if(err == false)
    {
        //alert("pride")
        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                function csrfSafeMethod(method) {
                    // these HTTP methods do not require CSRF protection
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    var csrftoken = getCookie('csrftoken');
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });


        //ajax klic za avtentikacijo
        $.ajax({
                type:"POST",
                url:"register/",
                data: {
                    'Ime': Ime,
                    'Priimek': Priimek,
                    'opis': opis,
                    'email': email,
                    'geslo': geslo,
                    'tel':tel,
                    'upime':upime,
                },
                success: function(data){
                    //alert(data)
                    if(data == '0'){
                        status.innerHTML="Registracija uspešna";
                        status.style.color="green";
                        window.location.replace("/");
                    }
                    if(data == '1')
                    {
                        status.innerHTML="Težava pri procesiranju parametrov";
                        status.style.color="red";
                    }
                    if(data == '3')
                    {
                        status.innerHTML="Uporabniško ime je že zasedeno.";
                        status.style.color="red";
                    }
                }
        });

    }
    }