function dodaj() {



    //spr pove ali naj se izvede ajax klic ali ne
    let err = false;

    let konj_ime = document.getElementById('konj_ime').value;
    let pasmaID = document.getElementById('pasmaID').value;
    let starost = document.getElementById('starost').value;
    let visina = document.getElementById('visinaID').value;
    let rodovnik_bool = document.getElementById('rodovnik_bool').value;
    let jahljivost = document.getElementById('jahljivost').value;
    let rokovanje = document.getElementById('rokovanje').value;
    let konj_opis = document.getElementById('konj_opis').value;
    let poskodbe = document.getElementById('poskodbe').value;
    let bolezni = document.getElementById('bolezni').value;
    let rezultati = document.getElementById('rezultati').value;
    let link_konjeniska_zveza = document.getElementById('link_konjeniska_zveza').value;
    let regijaID = document.getElementById('regijaID').value;
    let lastnik = document.getElementById("lastnik").value;
    let cena = document.getElementById("cena").value;
    //checboxi

    let kategorija = document.getElementsByName("kategorija");
    let kategorije = "";

    let pregled = document.getElementsByName("pregled");
    let pregledi = "";

    let status=document.getElementById("status");
    //preveri obvezne atribute
    if(konj_ime.length == 0 || pasmaID == -1 || konj_opis.length == 0 || rodovnik_bool == -1 || jahljivost == -1 || rokovanje == -1)
    {
        status.innerHTML="Za oddajo morate izpolniti vsa polja";
        status.style.color="red";
        err = true;
    }

    //za checboxke preveri ali so bili vnešeni ali ne
    for(let i=0; i < kategorija.length; i++){
              if(kategorija[i].checked){
                   kategorije+= kategorija[i].value+"/";
              }
        }

    if(kategorije.length == 0)
    {
        status.innerHTML="Za oddajo morate označiti tudi v katere kategorije spada vaš konj ";
        status.style.color="red";
        err = true;
    }


    for(let i=0; i < pregled.length; i++){
              if(pregled[i].checked){
                   pregledi+= pregled[i].value+"/";
              }
        }

    if(pregledi.length == 0)
    {
        status.innerHTML="Za oddajo morate označiti tudi v katere preglede spada vaš konj ";
        status.style.color="red";
        err = true;
    }



    if(visina < 50 || visina > 400)
    {
       status.innerHTML="Višina konja ne sme biti manjša od 50 ali večja od  400cm";
       status.style.color="red";
       err = true;
    }

    if(starost < 1 || starost > 600)
    {
        status.innerHTML="Starost konja ne sme biti manjša od 1 meseca ali večja od 600 msesecev";
        status.style.color="red";
        err = true;

    }


    if(rodovnik_bool == 1 && document.getElementById("image_to_upload").value == "")
    {
        status.innerHTML="Če ste izbrali pri rodovniku Da morate obvezno dodati sliko rodovnika";
        status.style.color="red";
        err = true;
    }


    if(err == false)
    {
        //alert("pride")
        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
          jQuery.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });
          jQuery.noConflict();
            formdata = new FormData();
            jQuery.ajax({
                type:"POST",
                url:"klic_dodaj_konja",
                data: {
                    'konj_ime':konj_ime,
                    'pasmaID':pasmaID,
                    'starost':starost,
                    'visina':visina,
                    'bolezni':bolezni,
                    'rodovnik_bool':rodovnik_bool,
                    'jahljivost':jahljivost,
                    'rokovanje':rokovanje,
                    'konj_opis':konj_opis,
                    'poskodbe':poskodbe,
                    'rezultati':rezultati,
                    'link_konjeniska_zveza':link_konjeniska_zveza,
                    'regijaID':regijaID,
                    'lastnik':lastnik,
                    'pregledi':pregledi,
                    'kategorije':kategorije,
                    'cena':cena
                },
                success: function(data){
                     if(data == '1'){
                        status.innerHTML="Konj neuspešno dodan";
                        status.style.color="red";
                    }
                    if(data == '2'){
                        status.innerHTML="Težava pri dodajanju konja";
                        status.style.color="red";
                    }

                    d=data.split("/");

                    //če ni potrebe po rodovniku izvedi to:
                    //Konj_ID=d[1]
                    let Konj_ID = d[1];
                    if(d[0] == '0' && rodovnik_bool == 0)
                    {
                       window.location.replace("dodaj_slika_konj/"+Konj_ID)
                    }

                    //če je bilo označeno za rodovnik, dodaj še sliko rodovnika
                    else
                    {
                        //odaja slike za rodovnik
                        let file=jQuery("#image_to_upload").get(0).files[0];
                        if (formdata)
                        {
                            formdata.append("image", file);
                            formdata.append("ID_Konj",Konj_ID)
                            jQuery.ajax
                            ({
                                url: "dodaj_slika",
                                type: "POST",
                                data: formdata,
                                processData: false,
                                contentType: false,
                                success:function()
                                {
                                    window.location.replace("dodaj_slika_konj/"+Konj_ID)
                                }
                            });
                        }
                    }

                }
        });


    }
    }


