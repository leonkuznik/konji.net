function dodaj(id) {
    //spr pove ali naj se izvede ajax klic ali ne
    let err = false;

    let ID_Konj=id;
    let konj_ime = document.getElementById('konj_ime').value;
    let pasmaID = document.getElementById('pasmaID').value;
    let starost = document.getElementById('starost').value;
    let visina = document.getElementById('visinaID').value;
    //let rodovnik_bool = document.getElementById('rodovnik_bool').value;
    let jahljivost_bool = document.getElementById('jahljivost_bool').value;
    let jahljivost_opis = document.getElementById('jahljivost_opis').value;
    let rokovanje_bool = document.getElementById('rokovanje_bool').value;
    let rokovanje_opis = document.getElementById('rokovanje_opis').value;
    let konj_opis = document.getElementById('konj_opis').value;
    let poskodbe = document.getElementById('poskodbe').value;
    let bolezni = document.getElementById('bolezni').value;
    let rezultati = document.getElementById('rezultati').value;
    let link_konjeniska_zveza = document.getElementById('link_konjeniska_zveza').value;
    let regijaID = document.getElementById('regijaID').value;
    let lastnik = document.getElementById('lastnik').value;
    let cena = document.getElementById('cena').value;
    //checboxi
    let veterinarski_pregledi = document.getElementsByName("pregledi");
    let pregledi = "";

    let kategorija = document.getElementsByName("kategorija");
    let kategorije = "";


    let status=document.getElementById("status");
    //preveri obvezne atribute
    if(konj_ime.length == 0 || lastnik == -1 || pasmaID == -1 || jahljivost_bool == -1 || rokovanje_bool == -1 || konj_opis.length == 0)
    {
        status.innerHTML="Za oddajo morate izpolniti vsa polja";
        status.style.color="red";
        err = true;
    }

    //za checboxke preveri ali so bili vnešeni ali ne
    for(let i=0; veterinarski_pregledi[i]; i++){
          if(veterinarski_pregledi[i].checked){
               pregledi+= veterinarski_pregledi[i].value+"/";
          }
    }

    if(pregledi.length == 0)
    {
      status.innerHTML="Za spremembo morate označiti tudi v katere preglede spada vaš konj ";
      status.style.color="red";
      err = true;
    }
    for(let i=0; i < kategorija.length; i++){
              if(kategorija[i].checked){
                   kategorije+= kategorija[i].value+"/";
              }
        }

    if(kategorije.length == 0)
    {
        status.innerHTML="Za spremembo morate označiti tudi v katere kategorije spada vaš konj ";
        status.style.color="red";
        err = true;
    }


    if(err == false)
    {
        //alert("pride")
        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });

        $.ajax({
                type:"POST",
                url:"../klic_spremeni_konja",
                data: {
                    'ID_Konj':ID_Konj,
                    'konj_ime':konj_ime,
                    'pasmaID':pasmaID,
                    'starost':starost,
                    'lastnik':lastnik,
                    'visina':visina,
                    'bolezni':bolezni,
                    //'rodovnik_bool':rodovnik_bool,
                    'jahljivost_bool':jahljivost_bool,
                    'jahljivost_opis':jahljivost_opis,
                    'rokovanje_bool':rokovanje_bool,
                    'rokovanje_opis':rokovanje_opis,
                    'konj_opis':konj_opis,
                    'poskodbe':poskodbe,
                    'rezultati':rezultati,
                    'link_konjeniska_zveza':link_konjeniska_zveza,
                    'regijaID':regijaID,
                    'pregledi':pregledi,
                    'kategorije':kategorije,
                    'cena':cena,
                },
                success: function(data){
                    //alert(data)
                    if(data == '0'){

                        location.reload();
                    }
                     if(data == '1'){
                        status.innerHTML="Konj neuspešno spemenjen. Preverite ali so vnešeni vsi zahtevani podatki.";
                        status.style.color="red";
                    }

                }
        });

    }
    }

