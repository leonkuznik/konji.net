function izbrisi(id) {
        ID_Konj=id;
        //da ne prihaja do CSRFToken errorja(poslati ga moramo prej drugače ne deluje)
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });
 if (window.confirm("Ste prepričani, da želite izbrisati konja?")) {
        $.ajax({
                type:"POST",
                url:"izbrisi_konja",
                data: {
                    'ID_Konj':ID_Konj,
                },
                success: function(data){
                    if(data==0)
                    {
                        location.reload();
                    }
                }
        });

    }
    }
